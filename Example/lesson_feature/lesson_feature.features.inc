<?php
/**
 * @file
 * lesson_feature.features.inc
 */

/**
 * Implements hook_node_info().
 */
function lesson_feature_node_info() {
  $items = array(
    'lesson' => array(
      'name' => t('Lesson'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
